#include "PowerSupplyInterface.h"
#include <boost/program_options.hpp> //!For command line arg parsing
#include <cstdlib>
#include <iostream>
#include <stdexcept>

/*!
 ************************************************
 * Argument parser.
 ************************************************
 */
boost::program_options::variables_map process_program_options(const int argc, const char* const argv[])
{
    boost::program_options::options_description desc("Allowed options");

    desc.add_options()("help,h", "produce help message")("config,c",
                                                         boost::program_options::value<std::string>()->default_value("default"),
                                                         "set configuration file path (default files defined for each test) "
                                                         "...")
                                                         ("port,p",
                                                         boost::program_options::value<uint32_t>()->default_value(7000),
                                                         "portabc "
                                                         "...")("verbose,v", boost::program_options::value<std::string>()->implicit_value("0"), "verbosity level");

    boost::program_options::variables_map vm;
    try
    {
        boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
    }
    catch(boost::program_options::error const& e)
    {
        std::cerr << e.what() << '\n';
        exit(EXIT_FAILURE);
    }
    boost::program_options::notify(vm);

    // Help
    if(vm.count("help"))
    {
        std::cout << desc << "\n";
        exit(EXIT_SUCCESS);
    }
    return vm;
}

/*!
 ************************************************
 * Main.
 ************************************************
 */
int main(int argc, char** argv)
{
    boost::program_options::variables_map v_map = process_program_options(argc, argv);

    int serverPort = v_map["port"].as<uint32_t>();
    std::string configFileName = v_map["config"].as<std::string>();
    std::cout << "Port: " << serverPort << std::endl;

    //PowerSupplyInterface thePowerSupplyInterface(serverPort, v_map["config"].as<std::string>());
    if(configFileName == "default")
    {
        std::cout << "I'm not initializing any instrument" << std::endl;
        PowerSupplyInterface thePowerSupplyInterface(serverPort);
        thePowerSupplyInterface.startAccept();
        while(true) { std::this_thread::sleep_for(std::chrono::milliseconds(1000)); }
    }
    else
    {
        std::cout << "I'm initializing with the following configuration file: " << configFileName << std::endl;
        PowerSupplyInterface thePowerSupplyInterface(serverPort, configFileName);
        thePowerSupplyInterface.startAccept();
        while(true) { std::this_thread::sleep_for(std::chrono::milliseconds(1000)); }
    }

    //thePowerSupplyInterface.startAccept();
    //while(true) { std::this_thread::sleep_for(std::chrono::milliseconds(1000)); }

    return EXIT_SUCCESS;
}
