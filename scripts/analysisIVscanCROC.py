#!/usr/bin/python3
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import os, errno
import glob
import pathlib
import argparse
import xml.etree.ElementTree as ET
import csv
from analysis_functions import *

####################
# Analyze function #
####################

def analyze(fileFolder, fileBaseName, minVoltageVal, maxVoltageVal, minCurrentVal, maxCurrentVal, var, twoChannelsFlag, labelArray, rExtArray, kFac, resultWriter, VinDid, VinAid):

    fileSaveDir     = fileFolder   + 'Images/'
    fileSaveName    = fileSaveDir  + fileBaseName
    fileSaveNameK   = fileSaveName + '_kfactor'
    fileName        = fileFolder   + fileBaseName + '.csv'
    fileType        = 'SingleCROC'

    resultLine      = []
    if resultWriter is not None:
        acqDayString    = (fileFolder.split(os.sep))[-2]
        acqDaySplitStr  = acqDayString.split('_')
        acqHourSplitStr = fileBaseName.split('_')
        acqDayAndTime = acqDaySplitStr + acqHourSplitStr
        posixClass=posixTS(acqDayAndTime)
        timeStamp=posixClass.getTS()
        resultLine.append(timeStamp)

    try:
        os.makedirs(fileSaveDir)
    except FileExistsError:
        pass

    df = pd.read_csv(fileName)
    df = df.sort_values(by=['CurrentD'], ascending=True)
    df = df.sort_values(by=['CurrentA'], ascending=True)

    print("Analyzing file " + fileName)
    #print("Analyzing with type " + fileType)

    if fileType == 'SingleCROC':

        # Plot variable
        yMin = -0.1
        yMax = 2.5
        xMin = 0.0
        xMax = 2.1

        # Final configurations
        figureWidth  = 8
        figureHeight = 4.8

        # Variables
        valueList       = [None]*(len(var)-1)
        valuePltList    = [None]*len(valueList)
        IArray          = [None]*2
        VinPSArray      = [None]*2
        psPltArray      = [None]*2
        ivLinePltArray  = [None]*2
        ivShortPltArray = [None]*2
        textstrArray    = [None]*2
        currentOVP      = [-1]*2
        voltageOVP      = [-1]*2
        I               = 0
        VinPS           = 0
        textstr         = ''

        # Getting data
        for v in range(0,2):
            varNameCurrent                                         = 'Current' + labelArray[v]
            varNameVoltage                                         = 'Vps'     + labelArray[v]
            varNameLabel                                           = 'VinPS'   + labelArray[v]
            IArray[v]                                              = np.array(df[varNameCurrent].tolist())
            VinPSArray[v]                                          = np.array(df[varNameVoltage].tolist())
            if not twoChannelsFlag:
                IArray[v] = np.divide(IArray[v],2)
            I                                                      = I     + (IArray[v]     / 2)
            VinPS                                                  = VinPS + (VinPSArray[v] / 2)
            #psPltArray[v] ,                                        = plt.plot(IArray[v], VinPSArray[v], 'o', label = varNameLabel )
            
            if VinDid is None or VinAid is None:
                psPltArray[v] ,                                        = plt.plot(IArray[v], VinPSArray[v], 'o', label = varNameLabel, alpha=0.5 )
                ivLinePltArray[v], ivShortPltArray[v], textstrArray[v] = ivCurveFit(IArray[v], VinPSArray[v], minVoltageVal, maxVoltageVal, minCurrentVal, maxCurrentVal, lineColor = psPltArray[v].get_color() , label = labelArray[v])
                textstr                                                = textstr + textstrArray[v] + '\n'
                if resultWriter is not None:
                    debuggingPrint('Single channel - retrieving offset and slope from string:', textstrArray[v], args.verbosity,1)
                    resultSlope = textstrArray[v].split('$')[1].split('=')[1]
                    resultOff   = textstrArray[v].split('$')[3].split('=')[1]
                    varNameRext = 'Vrext' + labelArray[v]
                    idx         = (np.abs(IArray[v] - 0.9)).argmin()
                    vRext       = (np.array(df[varNameRext].tolist()))[idx]
                    resultLine.append(resultSlope)
                    resultLine.append(resultOff)
                    resultLine.append(vRext)
        
        for i in range(1,len(var)):
            valueList[i-1] = np.array(df[var[i]].tolist())

        for i in range (0,len(valueList)):
            valuePltList[i] , = plt.plot(I, valueList[i], 'o', label = var[i+1], alpha=0.5)
       
        if (VinDid is not None and VinAid is not None):
                ivLinePltArray[0], ivShortPltArray[0], textstrArray[0] = ivCurveFit(I,valueList[VinDid], minVoltageVal, maxVoltageVal, minCurrentVal, maxCurrentVal, lineColor = valuePltList[VinDid].get_color(), label = "D")
                textstr                                                = textstr + textstrArray[0] + '\n'
                if resultWriter:
                    debuggingPrint('Two channels - retrieving offset and slope from string:', textstrArray[0], args.verbosity,1)
                    resultSlope = textstrArray[0].split('$')[1].split('=')[1]
                    resultOff   = textstrArray[0].split('$')[3].split('=')[1]
                    varNameRext = 'VrextD'
                    idx         = (np.abs(I - 0.9)).argmin()
                    vRext       = (np.array(df[varNameRext].tolist()))[idx]
                    resultLine.append(resultSlope)
                    resultLine.append(resultOff)
                    resultLine.append(vRext)
                
                #VovpD calculation
                resultSlope = textstrArray[0].split('$')[1].split('=')[1]
                for i in range(0,len(valueList[VinDid])-1):
                    if (valueList[VinAid][i] > minVoltageVal and I[i] > minCurrentVal):
                        increase = (valueList[VinDid][i+1] - valueList[VinDid][i]) / (I[i+1]-I[i])
                        if(increase < 0.8*float(resultSlope)):
                            print("OVP started above VinD[V]: ",valueList[VinDid][i]," and I[A]: ",I[i])
                            currentOVP[0] = I[i]
                            voltageOVP[0] = valueList[VinDid][i]
                            if(resultWriter):
                                resultLine.append(I[i])
                                resultLine.append(valueList[VinDid][i])
                            break
                
                ivLinePltArray[1], ivShortPltArray[1], textstrArray[1] = ivCurveFit(I, valueList[VinAid], minVoltageVal, maxVoltageVal, minCurrentVal, maxCurrentVal, lineColor = valuePltList[VinAid].get_color(), label = "A")
                textstr                                                = textstr + textstrArray[1] + '\n'
                if resultWriter:
                    resultSlope = textstrArray[1].split('$')[1].split('=')[1]
                    resultOff   = textstrArray[1].split('$')[3].split('=')[1]
                    varNameRext = 'VrextA'
                    idx         = (np.abs(I - 0.9)).argmin()
                    vRext       = (np.array(df[varNameRext].tolist()))[idx]
                    resultLine.append(resultSlope)
                    resultLine.append(resultOff)
                    resultLine.append(vRext)

                #VovpA calculation
                resultSlope = textstrArray[1].split('$')[1].split('=')[1]
                for i in range(0,len(valueList[VinAid])-1):
                    if (valueList[VinAid][i] > minVoltageVal and I[i] > minCurrentVal):
                        increase = (valueList[VinAid][i+1] - valueList[VinAid][i])/(I[i+1]-I[i])
                        if(increase < 0.8*float(resultSlope)):
                            print("OVP started above VinA[V]: ",valueList[VinAid][i]," and I[A]: ",I[i])
                            currentOVP[1] = I[i]
                            voltageOVP[1] = valueList[VinAid][i]
                            if(resultWriter):
                                resultLine.append(valueList[VinAid][i])
                            break

        for vIndex, vLinePlt in enumerate(ivLinePltArray):
            valuePltList.append(ivLinePltArray[vIndex])
            valuePltList.append(ivShortPltArray[vIndex])
        
        plt.legend(handles = valuePltList)

        props = dict(boxstyle = 'round', facecolor = 'gold', alpha = 0.8)

        # OVP line
        if (I[0] < 0.2):
            print("Writing ovp on plot")
            plt.axhline(y = voltageOVP[0], color = 'r', linestyle = '--')
            plt.axvline(x = currentOVP[0], color = 'r', linestyle = '--')
            plt.text(0.2, voltageOVP[0], 'OVP: ' + '{:.2f}'.format(voltageOVP[0]) + ' V', fontsize = 10, va='center', ha='center', backgroundcolor='w')
        
        plt.legend(
                #framealpha = 0,
                labelspacing    = 0.3,
                ncol            = 1,
                loc             = 'lower left',
                bbox_to_anchor  = (1, 0),
                )
        ax    = plt.gca()
        ax.set_ylim(yMin,yMax)
        ax.set_xlim(xMin,xMax)
        ax.text(
            1.03,
            1,
            textstr,
            transform           = ax.transAxes,
            fontsize            = 14,
            horizontalalignment = 'left',
            verticalalignment   = 'top',
            bbox                = props
            )
       
    # Title and labels
    plt.xlabel('Input Current per Channel [A]')
    plt.ylabel('Voltage [V]')
    plt.title('IV curve')

    # Showing and saving
    plt.gcf().set_size_inches(figureWidth, figureHeight)
    plt.tight_layout()
    plt.ylim([0,2.5])
    plt.grid()
    print("Saving image: "+ fileSaveName)
    plt.savefig(fileSaveName + '.pdf'                    , dpi = 1000)
    plt.savefig(fileSaveName + '.png', transparent = True, dpi = 1000)
    #plt.show()
    plt.close()

    if(kFac and VinDid is not None and VinAid is not None):# and resultWriter is not None):
        kFactorArray = [None]*2
        vIn          = [None]*2
        vRext        = [None]*2
        kPltArray    = [None]*2
        k            = [None]*2
        
        for v in range(0,2):
            varNameVoltage = 'Vin'   + labelArray[v]
            varNameRext    = 'Vrext' + labelArray[v]
            current = IArray[v]
            vIn     = np.array(df[varNameVoltage].tolist())
            vRext   = np.array(df[varNameRext]   .tolist())
            k[v]    = []
            for u in range(0,len(current)):
                k[v].append((current[u]*rExtArray[v])/(vIn[u]-vRext[u]))
        
            plt.figure()
            kPltArray[v] , = plt.plot(IArray[v], k[v], 'o', label = 'K - ' + labelArray[v])

            plt.xlabel('Input Current ' + labelArray[v] +' [A]')
            plt.ylabel('K Factor ' + labelArray[v])
            plt.title ('K Factor ' + labelArray[v])

            # Showing and saving
            plt.gcf().set_size_inches(figureWidth, figureHeight)
            plt.tight_layout()
            plt.ylim([0,5000])
            plt.grid()
            print("Saving image: "+fileSaveNameK + labelArray[v])
            plt.savefig(fileSaveNameK + labelArray[v] + '.pdf'                    , dpi = 1000)
            plt.savefig(fileSaveNameK + labelArray[v] + '.png', transparent = True, dpi = 1000)
        
            plt.close()

    if resultWriter:
        resultWriter.writerow(resultLine)

########
# Main #
########

def main(args):
    
    tree = ET.parse(args.configuration)
    root = tree.getroot()
    
    scannerCard = root.find('ScannerCard')
    
    var = []
    i = 1

    var.append(scannerCard.attrib.get('nChannels'))
    
    rextExists = 0
    j = 0
    VinDid=None
    VinAid=None 
    
    for i in range(1,int(var[0])+1):
        if scannerCard.attrib.get('Channel_' + str(i)):
            var.append(scannerCard.attrib.get('Channel_' + str(i)))
            j += 1
            if scannerCard.attrib.get('Channel_'+str(i))=="VinD":
                VinDid=j-1
            if scannerCard.attrib.get('Channel_'+str(i))=="VinA":
                VinAid=j-1
            if scannerCard.attrib.get('Channel_'+str(i))=="VrextD":
                rextExists+=0.5
            if scannerCard.attrib.get('Channel_'+str(i))=="VrextA":
                rextExists+=0.5
    
    if (VinDid is None or VinAid is None):
        print("\nWARNING: No VinD/A provided with the IV scan! Fits will be performed on VinPSD/A. OVP computation NOT available. Kfactor computation NOT available.\n") 
    if (rextExists<1): 
        print("\nWARNING: No VrextD/A provided with the IV scan! Fit data will NOT be saved. Kfactor computation NOT available.\n")


    analysis        = root.find('Analysis')
    powerSupply     = root.find('Devices').find('PowerSupply')
    rExtD           = float(analysis.attrib.get('RextD'))
    rExtA           = float(analysis.attrib.get('RextA'))
    minVoltageVal   = float(analysis.attrib.get('minVoltageVal'))
    maxVoltageVal   = float(analysis.attrib.get('maxVoltageVal'))
    minCurrentVal   = float(analysis.attrib.get('minCurrentVal'))
    maxCurrentVal   = float(analysis.attrib.get('maxCurrentVal'))
    kFac            = str_to_bool(analysis.attrib.get('kFactor'))
    read            = str_to_bool(analysis.attrib.get('ReadEverything'))
    two             = str_to_bool(powerSupply.attrib.get('TwoChannels'))
    
    folderForLoop       = args.data
    saveResultsFileName = folderForLoop + 'Results.csv'

    if (os.path.isfile(saveResultsFileName)):
        os.remove(saveResultsFileName)
    
    if read:
        fileList = glob.glob(folderForLoop + '[!imux]*.csv')
    else:
        fileList = glob.glob(folderForLoop + 'lastScan.csv')
    debuggingPrint('File list:', fileList, args.verbosity)

    labelArray = []
    rExtArray  = []
    labelArray.append('D')
    labelArray.append('A')
    rExtArray.append(rExtD)
    rExtArray.append(rExtA)

    vFileRes     = open(saveResultsFileName, 'a')
    resultWriter = csv.writer(vFileRes, delimiter = ',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    
    headerLine = []
    headerLine.append('TimeStamp')
    headerLine.append('SlopeD')
    headerLine.append('OffsetD')
    headerLine.append('VrextD')
    if (VinDid is not None and VinAid is not None):
        headerLine.append('Iovp')
        headerLine.append('VovpD')
    headerLine.append('SlopeA')
    headerLine.append('OffsetA')
    headerLine.append('VrextA')
    if (VinDid is not None and VinAid is not None):
        headerLine.append('VovpA')
    resultWriter.writerow(headerLine)


    for vFile in fileList:
        fileBaseName = pathlib.Path(vFile).stem
        if fileBaseName == 'lastScan':
            resultWriterTmp = None
        elif rextExists < 1:
            resultWriterTmp = None
        elif rextExists == 1:
            resultWriterTmp = resultWriter
        analyze(folderForLoop, fileBaseName, minVoltageVal, maxVoltageVal, minCurrentVal, maxCurrentVal, var, two, labelArray, rExtArray, kFac, resultWriterTmp, VinDid, VinAid)

    if vFileRes is not None:
        vFileRes.close()

###################
# Argument parser #
###################
parser = argparse.ArgumentParser(description='Plot of IV curve data using matplotlib and pandas')
parser.add_argument('-c' ,'--configuration',help = 'XML configuration file' ,default = '../config/iv_it_croc_sldo.xml', type = str  )
parser.add_argument('-d' ,'--data'         ,help = 'CSV results folder'     ,default = '../results/SP_Try/2022_04_04/', type = str  )
parser.add_argument('-v', '--verbosity'    ,help = 'Verbosity for debugging',default = 0                              , type = int )

args = parser.parse_args()

if __name__ == "__main__":
    main(args)
